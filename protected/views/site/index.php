<?php 
	$title = "ФИО";
	$continue_flag = false;

	if(isset($text_q)) {
		$title = "Вопрос <span id='q_number'>".$current_q."</span> / 10";
		$continue_flag = true;
	}
?>

<div class="container">
	<div class="row">
		<div class="span5 offset3">
			<fieldset>
				<legend> <?= $title; ?> </legend>

				<div id="fio-form" <?php if($continue_flag) echo 'style="display: none;"'; ?>>
					<label>ФИО</label>
					<input id="fio" type="text" class="center full-width" placeholder="ФИО">
					<label>Возраст</label>
					<input id="age" type="text" class="center full-width" placeholder="Возраст">
					<button class="btn" id="submit-fio-button">Начать</button>
					<button class="btn" id="results-button">Результаты</button>
				</div>

				<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Ошибка: </span>
					Поля формы не заполнены
				</div>
				
				<div id="question-container" <?php if($continue_flag) echo 'style="display: block;"'; ?>>
					<div id="text-question"> <?php if($continue_flag) echo $text_q; ?> </div>
					<button class="btn btn-small btn-answer" data="1"> <i class="icon-thumbs-up"></i> Да</button>
					<button class="btn btn-small btn-answer" data="0"> <i class="icon-thumbs-down"></i> Нет</button>	
				</div>
				
				<div id="result-block">
					<div class="result">Вы зависимы на: <span id="dep"></span>%</div>
					<div class="result">Вы общительны на: <span id="soc"></span>%</div>
					<div class="result">Вы борец на: <span id="fig"></span>%</div>		
				</div>
				
				<div id="table-container">
					<table class="table table-bordered">
						<thead>
					  		<th> Имя </th>
					  		<th> Возраст </th>
					  		<th> Зависимость </th>
					  		<th> Общительность </th>
					  		<th> Принятие борьбы </th>
					  		<th> Удаление </th>
						</thead>						
					</table>	

					<button class="btn" id="back-button">Назад</button>
				</div>
			</fieldset>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	var question_container = $("#question-container");
	var current_question = 1;	

	var COUNT_QUESTIONS = 10;

	$(document).ready(function() {		
		var number = $('#q_number').html();

		if(number) {
			current_question = number;
		}
	});

	// Клиент для работы с YII API
	var api = {

		GET_QUESTION_URL: "/index.php/questions/index/",
		ADD_USER_URL: "/index.php/users/add/",
		GET_USERS_URL: "/index.php/users/get/",
		DEL_USER_URL: "/index.php/users/delete/",


		getQuestion: function(index, result_callback, dict_args) {

			var args = {
				"id": index
			}

			args = merge_objects(args, dict_args);

			$.get(this.GET_QUESTION_URL, args, function(data) {
				result_callback($.parseJSON(data));
			})
		},

		addUser: function(result_callback) {
			$.post(this.ADD_USER_URL, function(data) {
				result_callback($.parseJSON(data));
			})
		},

		getUsers: function(result_callback) {
			$.get(this.GET_USERS_URL, function(data) {
				result_callback($.parseJSON(data));
			})
		},

		delUser: function(id, result_callback) {
			$.post(this.DEL_USER_URL, {
				"id": id
			}, function(data) {
				result_callback($.parseJSON(data));
			})
		}
	}

	// Отправка фио
	$("#submit-fio-button").on("click", function() {
		$(".alert").hide();
		var this_form = $("#fio-form");
		var age_value = this_form.children("#age").val();

		if(!this_form.children("#fio").val() || !age_value
			|| !$.isNumeric(age_value)) {
			$(".alert").show();
		} else {
			// В локле устанавливаем данные

			var info = {
				"name": this_form.children("#fio").val(),
				"age": this_form.children("#age").val()
			};

			// Подгружаем первый вопрос
			setContent(1, info);
		}
	});

	// Устанавливаем вопрос
	function setContent(question_index, name, age) {

		if($("#fio-form").is(":visible")) {
			$("#fio-form").hide();
		}
		question_container.show();

		api.getQuestion(question_index, function(data) {
			$("legend").html("Вопрос " + question_index + " / " + COUNT_QUESTIONS)			
			question_container.children("#text-question").html(data["text"]);
		}, name, age);
	}

	function addResult(callback) {
		$('legend').html("Результат");
		$('#question-container').hide();
		current_question = 0;
		api.addUser(function(data) {
				$("#result-block").show();
				$("#dep").html(data["dependence"]);
				$("#soc").html(data["sociability"]);
				$("#fig").html(data["fight"]);
				callback();
			}
		);
	}

	function getAllUsers() {
		$('#table-container').show();

		api.getUsers(function(data) {

			for (var property in data) {

				var answers_arr = data[property]['answer'].split(',');

				// Выглядит ужасно, тут бы зашел Mustache (но лень)
				var append_str = "<tbody> <th>" + data[property]['name'] + "</th>" +
		   		"<th>" + data[property]['age'] + "</th>";

		   	for(var i = 0; i < answers_arr.length; i++) {
		   		append_str += "<th>" + answers_arr[i] + "</th>";
		   	}

		   	append_str += "<th> <span class='del' data='" + property +
		   		"'> Удалить </span> </th> </tbody>";

		   	$('.table').append(append_str);
			}

			$(".del").on("click", function() {
				var td = $(this).parent();
				var tr = td.parent();

				var id = $(this).attr("data");				
				api.delUser(id, function(data) {
					if(data["ok"] == 1) {
						tr.remove();
					}
				})
			});
		});
	}

	$('.btn-answer').on('click', function() {
		current_question++;
		setContent(current_question, {"answer": $(this).attr("data") + ","});

		if(current_question > COUNT_QUESTIONS) {
			// Кастыльно добиваюсь синхронности :3
			addResult(getAllUsers);			
			current_question = 1;		
		}
	});

	$('#results-button').on('click', function() {
		$("#fio-form").hide();
		$('legend').html("Результат");
		getAllUsers();
	});

	$('#back-button').on('click', function() {
		$('legend').html("ФИО");
		$('#table-container').hide();
		$('#result-block').hide();
		$("#fio-form").show();
		$('tbody').remove();
	});

	function merge_objects(obj1, obj2) {
		var obj3 = {};
		for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
		for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
		return obj3;
	}

</script>




