<?php

// This is the database connection configuration.
return array(	
	'connectionString' => 'mysql:host=localhost;dbname=test_stenf',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
);