<?php

class SiteController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$session=new CHttpSession;	
		$session->open();	

		$arr_params = array();
		$id_q = $session["current_q"];

		if(isset($id_q)) {

			$quest = Questions::model()->findByPk($id_q + 1);

			if($quest) {
				$arr_params["current_q"] = $session["current_q"] + 1;
				$arr_params["text_q"] = $quest["question"];
			}
		}

		$session->close();

		$this->render('index', $arr_params);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}