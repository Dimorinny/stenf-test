<?php

class QuestionsController extends Controller {
	// Метод получает id вопроса и возвращает информацию в JSON
	public function actionIndex() {

		$id = Yii::app()->request->getParam('id');

		$name = Yii::app()->request->getParam('name');
		$age = Yii::app()->request->getParam('age');
		$answer = Yii::app()->request->getParam('answer');

		$session=new CHttpSession;
		$session->open();
		
		$session["answers"] .= $answer;

		// Если только пришли и дали данные, то пишем в сессию
		if(isset($name) && isset($age)) {
			$session["name"] = $name;
			$session["age"] = $age;			
		} else {
			$session["current_q"] += 1;
		}

		$session->close();

		$question = Questions::model()->findByPk($id);

		if(is_null($question)) {
			throw new CHttpException(404,'Post not found.');
		}

		$resultJson = [
			"id"=> $question["id"],
			"text"=>$question["question"]
		];

		echo json_encode($resultJson);
	}
}

?>
