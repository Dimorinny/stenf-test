<?php

class UsersController extends Controller {
		
	private $dependence = [3, 9];
	private $sociability = [5, 7];	
	private $fight = [1];

	public function filters() {
		return array(
			'postOnly + add, delete'
		);
	}

	// Выщитываем результат теста и создаем юзера
	public function actionAdd() {

		$session=new CHttpSession;
		$session->open();
		
		$name = $session['name'];
		$age = $session['age'];
		$answers = $session['answers'];

		$answers = rtrim($answers, ",");
		
		$results = array(
			"dependence" => 0,
			"sociability" => 0,
			"fight" => 0
		);

		$matched_count = 0;

		$array_answers = explode(",", $answers);

		// Ужасный алгоритм по выщитыванию процентов
		foreach ($array_answers as $key => $value) {
			$key++;
			if(intval($value) != 0) {	
				$in_dependence = in_array($key, $this->dependence) ? 1 : 0;
				$in_sociability = in_array($key, $this->sociability) ? 1 : 0;
				$in_fight = in_array($key, $this->fight) ? 1 : 0;

				$results["dependence"] += $in_dependence;
				$results["sociability"] += $in_sociability;
				$results["fight"] += $in_fight;

				if($in_dependence || $in_sociability || $in_fight) {
					$matched_count++;		
				}
			}
		}

		$percent = intval(100 / $matched_count);

		foreach ($results as &$value) {
			$value *= $percent;
		}

		$user = new User;

		$user->name = htmlspecialchars($name);
		$user->age = $age;
		$user->answer = implode(",", array_values($results));
		$user->save(true);

		$session->destroy();
		$session->close();

		echo json_encode($results);
	}

	public function actionGet() {
		$all = User::model()->findAll();

		$arr = [];

		foreach ($all as $value) {
			$arr[$value->id] = array(
				"name" => $value->name,
				"age" => $value->age,
				"answer" => $value->answer
			);
		}

		echo json_encode($arr);
	}

	public function actionDelete() {

		$id = Yii::app()->request->getParam('id');
		$result = array("ok" => 0);
		$results["ok"] = User::model()->deleteByPk($id);
		echo json_encode($results);
	}
}